---
title: "全链路性能测试SuperSheeps用例详解"
meta: <meta name="description" content="基于录制回放的游戏服务器及web性能测试专家"/>
---
## SuperSheeps用例详解

![QQ截图20230417203713](/assets/QQ截图20230417203713_lg3cg1k5x.png)

supersheeps初始是为支持使用二进制rpc的长连接游戏服务器而设计，并且为了便捷生成测试用例，使用了socks5代理服务器录制游戏客户端网络事件，后又添加支持http和https代理服务器。

为了其通用性，用例中存储的为传输层（TCP、UDP）事件：连接、发送、关闭  
```
连接： 该事件记录了对应的IP、端口以及传输协议，TCP或者SSL，UDP不会产生此事件   
关闭： 该事件记录了对应的IP、端口以及传输协议，TCP或者SSL，UDP不会产生此事件  
发送： 该事件记录了对应的IP、端口、传输协议，以及发送的二进制字节流， 该字节流会以Base64的方式，写入数据库，执行任务时会自动还原成二进制。
```

录制用例时需要将相应游戏客户端或者浏览器，设置好代理，所有事件及其对应的信息都会存储到sqlite数据库中。

### 1. 数据库说明
![QQ截图20230417204022](/assets/QQ截图20230417204022.png)
#### 1.1 表：**record_xxx_xxx_xxx_xxx$yy**

|id|recordtime|event_type|protocol|ip|port|sessionid|content|note|
|---|---|---|---|---|---|---|---|---|
|1|1151|0|0|www.baidu.com|80|744|||
|2|2151|1|0|www.baidu.com|80|744|base64_str||
|3|3151|2|0|www.baidu.com|80|744|||

id:         自增id,无意义  
recordtime: 微秒时间戳，用于用例步骤排序  
event_type: 事件类型，连接、发送或关闭  
protocol：  传输协议，0 TCP、1 UDP、2 SSL、8 HTTP  
ip:         网络地址或者域名  
port：      端口  
sessionid:  会话id，存在多个相同protocol、ip、port的连接时，用此区分，为http短连接场景而设计  
content：   base64字符串，用于兼容存储二级制消息  
note：      批注，默认空。使用自定义的方式生成用例时，可以用于存储一些特殊参数，由此执行特殊规则。

#### 1.2 表：**caseinfo**

|id|project_id|project_name|des|
|---|---|---|---|
|1|0|Demo|用例说明|

id:             自增id，无意义  
project_id:     项目id  
project_name:   项目名称  
des：           用例说明  

### 2. 录制用例

用户针对不同应用使用不同的方式录制用例。

#### 2.1 web应用
针对此类场景可以使用http代理录制用例，浏览器一般都支持配置http或者https代理，根据不同的浏览器进行相应配置即可。
如google chrome浏览器在windows10下，“设置->系统->打开计算机的代理设置”，会跳转至系统代理服务器配置。在手动设置代理下，打开“使用代理服务器”，然后配置supersheeps控制端ip及端口(默认1080)。  
android系统，则可以在“设置->WLAN->长按已连接的wifi热点->修改网络，代理项选择手动，然后填写supersheeps控制端ip及端口。

#### 2.2 PC应用

一般此类应用不使用http协议，可以使用socks5或者https代理进行录制，但此类应用配置客户端代理需要第三方工具配合。

如windows下的游戏客户端，可以使用proxifier、ProxyCap等第三方工具。

#### 2.3 android应用

方法一借助android模拟器在PC上进行录制，方法同PC应用，此时录制的是模拟器进程。

方法二是对手机或者android模拟器进行root，安装第三方客户端代理配置工具。如ProxyDroid

#### 2.4 应用集成

部分用户可能觉得使用第三方工具不太方便，如果是自己研发的应用，可以选择在自己的应用中集成录制功能。
对此supersheeps控制端提供了http接口，可以把用户自身应用捕获到的用例事件，转换成supersheeps可以使用的用例。

```
POST /api/case_create 
json参数：{"project_id": 0, "des": "用例说明信息"}
json返回：{"case_name": "xxxxx"}

POST /api/case_append
json参数：{"case_name": "xxxxx", "recordtime":1664526989654, "event_type":1, "protocol":0, "host":"127.0.0.1", "port":80, "sessionid":0, "content":"base64_str", "note":""}
json返回：{"ret": "OK"}
```

#### 附：客户端代理工具推荐

windwos：
1. proxifier: socks5代理工具 [http://soft.onlinedown.net/soft/971579.htm]
2. proxycap：socks5代理工具，支持UDP [http://www.proxycap.com/]

android:
1. proxydroid: socks5代理工具 [https://pan.baidu.com/s/1dlLNS8Sajhmce_e7eD-fIg] 提取码：1234