---
html:
    toc: true
    embed_local_images: true
title: "全链路性能测试SuperSheeps用户使用手册"
meta: <meta name="description" content="基于录制回放的游戏服务器及web性能测试引擎"/>
---
# SuperSheeps用户使用手册

## 1.简介

&emsp;&emsp;SuperSheeps(习谱压力测试软件)是通用于TCP、UDP、SSL、KCP服务器的压力、性能测试引擎，支持IPv4、IPv6双栈。  基于对用户行为产生的网络事件进行录制和回放，产生大量用户模拟操作产生的网络连接，发送的数据包。使用类似播放音视频的回放设计，回放过程可控制播放、暂停、快进状态。可用于游戏、Web、物联网、视频推流等服务器的压力、性能测试。应用于项目研发阶段进行开发调试，发现代码缺陷。线上容量评估，提前规划服务器集群部署。
&emsp;&emsp;因其使用录制回放的方式进行压力测试，类似羊群效应，初始版本OpenSheeps已经代码开源，此版本为加强版，特命英文名为SuperSheeps。又因其核心设计类似音视频播放器，特中文命名为习谱压力测试软件。  
&emsp;&emsp;本系统支持单机、分布式部署，使用C++开发核心框架，内置lua虚拟机，默认使用lua脚本扩展项目支持，也支持javascript、python及golang。目前推荐使用lua开发，简单且高效。（*维护精力有限，当前版本移除对python、javascript的相关支持*）

### 1.1 网络协议支持
&emsp;&emsp;引擎通过自研高性能异步actor网络模型，同时支持TCP和UDP传输协议，并扩展支持了基于TCP的SSL和基于UDP的KCP协议，进一步扩展支持Http1.1和Websocket，并支持扩展其他应用层协议，如Sproto、Protobuf，用户有需要可自行添加。

### 1.2 压测运行原理
&emsp;&emsp;SuperSheeps采用录制回放的的工作方式。使用http(s)/socks5代理的方式录制客户端网络事件，形成一个有序队列。并把回放过程抽象为一个播放器，每个用例步骤为一个播放帧，每一帧都调用用户自定义的逻辑处理过程，同时用户可以控制播放器状态以实现播放器播放、暂停、快进、回退。  
&emsp;&emsp;Supersheeps内部提供网络、播放控制、数据上报、日志输出等接口。

```lua
--伪代码
EventStart()  --开始
while(true){
    EventTimeout()
    event = next_event()
    if player_state != stop && event != nil then
        case event:
            -- EventConnectOpen() ➡ {处理业务逻辑；player_state = pause;}
            -- EventConnectMade() ➡ {处理业务逻辑；player_state = play;}
            -- EventConnectFaild() ➡ {处理业务逻辑；player_state = stop;}
            -- EventConnectSend() ➡ {处理业务逻辑；player_state = pause;}
            -- EventConnectRecved() ➡ {处理业务逻辑；player_state = play;}
            -- EventConnectClose() ➡ {处理业务逻辑；player_state = stop;}
            -- EventConnectClosed() ➡ {处理业务逻辑；player_state = stop;}
    end
}
EventStop() --结束
```

## 2.使用

### 2.1 启动运行

&emsp;&emsp;cmd窗口下，执行命令 supersheeps.exe ?，或者直接双击运行supersheeps.exe，默认启动单机模式
访问<http://127.0.0.1:1080> ，查看web后台

```bash
可选参数说明：-m [] -ip [] -port [] -l [] -s [] -c [] -p [] -g []
-m:	启动模式,c-负载端模式,s-控制端模式,cs-单机模式
-ip:	指定控制端ip地址
-port:	指定控制端端口
-l:	指定负载端使用脚本语言,默认lua
-s:	指定负载端脚本文件
-c:	指定负载端ini配置文件
-p:	指定负载端项目id
-g:	指定负载端组id

单机示例：  supersheeps.exe -m cs
控制端示例：supersheeps.exe -m s -port 1080
负载端示例：supersheeps.exe -m c -p 0
```

### 2.2 目录说明

```dir
supersheeps.exe     -主程序(windows)
supersheeps         -主程序(linux)
Sheeps.dll          -Supersheeps核心动态库(windows)
libsheeps.so        -Supersheeps核心动态库(linux)
sheeps.ini          -配置文件

case                -用例目录
log                 -日志目录
tasklog             -控制端日志回收存放目录
liblua              -lua库存放目录
libpython           -python库存放目录
libjs               -javascript库存放目录
project/lua         -lua脚本目录，用户的lua代码放在这里
project/lua/0       -0号项目专属目录，以项目id命名，以此类推，lua模式下有效
project/python      -python脚本目录，用户的python代码放在这里
project/js          -javascript脚本目录，用户的javascript代码放在这里
report              -测试报告保存目录
server              -控制端扩展lua脚本目录
```

特殊说明：lua模式下，supersheeps初始化lua虚拟机时会自动将project/lua目录及其子目录添加到lua虚拟机package.path，但排除非当前项目专属目录。

### 2.3 部署

1. 控制端：supersheeps.exe -m s
2. 负载端：supersheeps.exe -m c
3. 单机运行： supersheeps.exe -m cs
4. 分布式运行：先一台机器上启动控制端，记录ip地址和端口。然后分别在不同机器上部署SuperSheeps程序，在sheeps.ini中配置控制端ip地址和端口，以负载端模式启动。

注：单机模式即在同一个进程内同时运行控制端和负载端。分布式与单机模式无本质区别，使用上也无区别，控制端内部会自动执行负载均衡，用户无需关注并发细节，仅需要关心业务和测试策略。

## 3.基本概念

### 3.1 控制端

&emsp;&emsp;SuperSheeps服务端，用于用例管理，任务管理等，并提供扩展支持  
&emsp;&emsp;详细内容阅读【7.控制端扩展】

#### 3.1.1 压测系统核心服务

&emsp;&emsp;管理测试用例及测试任务，与负载但保持tcp长连接，下发测试指令，收集测试数据生成测试报告，支持配置多个项目。

#### 3.1.2 Web静态和动态接口服务

&emsp;&emsp;内置web静态文件服务，可以托管html静态页面及其他文件，并设置首页，并且可以通过lua脚本处理任意http动态请求。

#### 3.1.3 网络代理服务

&emsp;&emsp;内置http(s)及socks5代理服务，用于用例录制

#### 3.1.4 脚本扩展支持和钩子函数

&emsp;&emsp;支持使用lua代码编写web动态接口或者高自由度用户脚本，以实现其他功能和服务。钩子函数可以接收SuperSheeps内部事件，以便用户对接第三方系统或者实现自定义功能

#### 3.1.5 前后端分离设计

&emsp;&emsp;系统内部自带前端系统，因为使用前后端分离设计，如果需要，使用者可以重写前端，并利用系统自带web静态文件服务能力部署。

### 3.2 负载端

&emsp;&emsp;SuperSheeps客户端，接收压测指令，执行压测任务，运行用户代码，上报测试数据

#### 3.2.1 用户模拟和网络并发

&emsp;&emsp;内部集成IO多路复用及高性能定时器，以定时器周期性驱动虚拟用户工作，实现actor网络模型，轻松模拟模拟上万用户实现海量网络并发

#### 3.2.2 项目接入

&emsp;&emsp;使用者仅需要根据自身项目，为单个虚拟用户编写业务逻辑代码，执行测试时内部会自动复制出指定数量的用户。内部还实现了一组网络接口、日志接口、播放控制接口、测试数据上报接口供用户调用。  
&emsp;&emsp;负载端默认使用lua语言开发，同时支持python和golang，建议初学者使用lua开发和学习，了解其工作机制后如有需要再尝试使用其他语言。

#### 3.2.3 播放器设计

&emsp;&emsp;系统采用录制回放的方式工作，但对播放采用类似播放音视频的设计，每个虚拟用户由一个单独的播放器驱动，每一个用例步骤为一个播放帧，帧与帧之间的时间间隔与录制时一致，每一帧都调用项目层代码处理其业务逻辑和参数，执行过程中可以切换播放器状态为播放、暂停、快进，甚至可以回退。这个设计的好处是将业务中需要等待服务器返回时，则将播放器设置为暂停状态。

#### 3.2.4 静态IP支持

&emsp;&emsp;通常情况下因为TCP协议限制，端口最大为65535，也就是说单机最大并发连接数最大不超过65535个，使用静态IP支持，可以突破此限制。原理这是将本机设置为静态IP地址，并添加多个有效IP地址，将这些IP地址写入ips.txt文件，每行一个ip地址，放入程序根目，在控制台或者通过修改sheeps.ini配置static_ip为1开启。

#### 3.2.5 分布式部署

&emsp;&emsp;单机物理性能有限的情况下，可以使用分布式部署扩展系统并发能力。使用者只需要启动多个负载端，并连接到同一个控制端即可，控制端会自动将用户平均分配到所有机器上。

#### 3.2.6 资源同步

&emsp;&emsp;系统内部实现了资源同步功能，将控制端project目录同步至负载端本地目录，分布式部署时可以快速下发代码配置到各机器。

#### 3.2.7 项目及分组设计

&emsp;&emsp;从平台化设计出发，当有多人同时执行不同测试任务，为了不相互干扰，负载端启动时会告诉控制端自身所属的项目及执行组，负载端只会执行指定项目及分组的任务。

### 3.3 测试用例

&emsp;&emsp;SuperSheeps使用的测试用例是一组描述单个用户网络事件有序集合，其存储于sqlite生成的db文件中。其中包含三种网络事件：连接、发送、关闭。

    event_type：    打开连接 0、发送消息 1、关闭连接 2  、接收消息 3(未使用)
    record_time：   事件记录时间，微秒
    protocol：      TCP 0、UDP 1、SSL 2、HTTP 8、HTTPS 9 
    ip：            服务器IP地址，也可能是域名
    port：          服务器端口号
    sessionid:      会话id
    conntet：       发送事件消息，base64编码存储，运行过程中会转换为字节串，用户解析时自行处理粘包  
    note:           用例步骤批注

### 3.4 测试任务

&emsp;&emsp;SuperSheeps通过测试任务组织测试活动，测试任务描述测试过程使用的测试用例、测试策略，以及测试参数。

    说明：任务文字描述  
    项目：可配置  
    人数：当前任务需要模拟的总用户数量  
    单次：每次启动的用户数量，不会超过配置的总用户数  
    间隔：每次启动用户之间的时间间隔  
    循环：单个用户用例执行完毕是否重新开始。实时回放模式下录制和回放可以同时进行，用例必须先择default.db。  
    忽略错误：循环模式下用户异常停止后是否重新开始执行。
    日志：任务运行时用户日志输出等级。
    日志回收：任务结束将负载端任务日志文件上传至控制端
    用例：用例文件，例如data.db  
    回放端口：用例文件中可以回放的数据，勾选则生效，不勾选不生效
    附加参数-自定义：非必须参数，格式为a=1&b=hello  
    附加参数-预定义：非必须参数，在sheeps.ini中配置

## 4.测试实施

    用例准备 → 负载端项目接入（开发与调试） → 测试执行 → 报告分析

### 4.1 用例准备

&emsp;&emsp;SuperSheeps目前提供了两种方式生成用例，录制和手动编辑。

#### 4.1.1 用例录制
&emsp;&emsp;SuperSheeps控制端内置http、https、socks5代理服务，端口默认统一为1080，通过**代理服务器录制目标项目客户端**产生的网络行为，保存到db文件中，即为测试用例。
&emsp;&emsp;http和https代理仅支持录制对应通讯协议的流量，socks5则支持TCP、UDP及衍生的所有通讯协议的流量
&emsp;&emsp;如需要录制https明文流量，请在“web后台——关于SuperSheeps”页面下载根证书，并安装受信任列表中，并且在sheeps.ini中配置proxy_ssl_ports。  
&emsp;&emsp;代理设置好后，在web后台用例管理下开启录制功能，结束后关闭录制，保存用例。设置代理，根据项目类型和使用的工具而定。 

#### 4.1.2 http用例

&emsp;&emsp;SuperSheeps控制端内置http用例编辑器，可以自由编辑保存http用例。

### 4.2 负载端项目接入

&emsp;&emsp;在web后台任务管理下选定测试用例、配置测试策略，总人数设置为1人，点击**添加**。然后在刚添加的任务中点击**开始**，即可进行运行调试。  
&emsp;&emsp;详细内容阅读【5.负载端项目接入（脚本开发与调试）】

### 4.3 测试执行

&emsp;&emsp;项目接入完成后，重新配置任务策略，执行压测任务。

### 4.4 报告分析

&emsp;&emsp;任务运行过程中可以查看实时测试报告，含有在线人数统计，连接统计，网络流量统计，数量统计，接口流量及响应耗时统计，运行结果概要统计。其中数量统计和接口流量及响应耗时统计为项目接入时自定义开发，内部提供有相应接口供开发者调用。

## 5.负载端项目接入（脚本开发与调试）

&emsp;&emsp;用户无需关心多用户并发及网络并发处理，SuperSheeps内部已经实现一套性能极高的并发方案，框架提供了网络接口和报表接口。用户只需根据项目，完成单用户业务逻辑开发即可。需要说明的是，因为SuperSheeps的特殊工作原理，负载端的默认工作仅仅是把录制的用例重新执行一次，并且**每一步都调用用户脚本执行逻辑处理**。

### 5.1 脚本支持

&emsp;&emsp;SuperSheeps默认使用lua作为扩展脚本支持，并且可以使用python/golang，其他语言会视情况逐步支持

### 5.2 播放器

&emsp;&emsp;SuperSheeps是基于录制回放的工作方式，其负载端内部实现了用例播放器，每一个并发用户在运行过程中都受自身的播放器控制，播放器状态——播放、暂停、快进或者回退。

    播放：播放器顺序执行的执行用例步骤，直至结束
    暂停：暂停执行用例，但是会执行播放超时事件
    快进：无视用例步骤之间时间间隔控制 ，在播放器下一个工作周期立即执行下一个用例步骤
    回退：回退到指定的用例步骤

### 5.3 播放事件

&emsp;&emsp;播放事件包含有开始、停止、超时事件，以及用例事件。在模拟用户开始和停止运行时分别调用一次，用例事件则是在运行过程中依据测试用例顺序执行的一系列行为

    开始事件：EventStart，模拟用户初始化执行一次
    停止事件：EventStop，模拟用户结束运行执行一次
    超时事件：EventTimeOut，模拟用户根据定时器周期触发的事件，处理一些周期性的逻辑
    用例事件：EventConnectOpen、EventConnectSend、EventConnectClose， 用例中包含的三类事件，连接、发送、关闭

### 5.4 网络事件

&emsp;&emsp;用例执行过程中，模拟用户按照用例事件执行的网络连接和发送操作，均为异步处理。SuperSheeps内部使用异步网络IO，运行过程中会产生四种网络事件，连接成功、失败、关闭及消息接收完成事件

    连接成功：EventConnectMade，网络连接成功
    连接失败：EventConnectFailed，网络连接失败
    连接关闭：EventConnectClosed，网络连接中止
    连接消息：EventConnectRecved，网络连接接受到消息

### 5.5 Lua模式

&emsp;&emsp;SuperSheeps集成lua虚拟机，用户可以使用lua脚本用于逻辑开发（*禁止在脚本中使用协程）。

#### 5.5.1 用户并发管理

&emsp;&emsp;SuperSheeps为每个模拟用户分配一个虚拟机，这样确保每个用户的数据相互独立，不会相互干扰。同时还存在一个全局虚拟机。在虚拟机被创建时会调用其初始化函数。

    全局虚拟机调用 global_init(project_id)
    用户虚拟机调用 luavm_init(project_id)
    这两个函数定义在sheeps.lua文件中，非必要无须处理，sheeps.lua为lua模式下框架的一部分。

#### 5.5.2 集成第三方库

&emsp;&emsp;目前已经内部集成第三方lua库，用户可以直接使用。

    已支持的有lua-protobuf、sproto、lpeg、websocket、skynet的crypt

#### 5.5.3 lua项目模板

&emsp;&emsp;在项目代码中实现播放、用例及网络事件接收函数，共10个，请确认名称和接收的参数正确，否则运行可能出错，最简单的方法就是将示例代码复制到项目中，例如main.lua

```lua
 --file: main.lua
function EventStart(task)
end

function EventConnectOpen(ip, port, protocol)
end

function EventConnectMade(hsock)
end

function EventConnectFailed(hsock, err)
end

function EventConnectClose(ip, port, protocol)
end

function EventConnectClosed(hsock, err)
end

function EventConnectSend(ip, port, data, protocol)
end

function EventConnectRecved(hsock, data)
end

function EventTimeOut()
end

function EventStop(errmsg)
end
```

#### 5.5.4 lua接口

&emsp;&emsp;supersheeps接口总共分为四大类，1.辅助类; 2.连接操作类; 3.播放器控制类; 4.报表类

##### 5.5.4.1辅助类接口
```lua
Log(loglevel, ...)

打印用户日志
参数：
loglevel: 日志等级，0 LOG_TRACE、1 LOG_DEBUG、2 LOG_NORMAL、3 LOG_ERROR、4 LOG_FAILT
...:    可变参数
返回值：
无
```

```lua
Microsecond()

获取一个64位微秒时间
参数：无
返回值： 
number 类型
```

```lua
UserActiveTimeOutStop(timeout)

设置用户活动超时时间，强制停止执行
参数：
timeout: number 类型
返回值： 无
```

```lua
FunctionRegist(name, func, ...)

注册一个函数,并指定任意个参数，此函数会随用户定时器周期执行。（仅lua模式使用）
参数：
name: 名称,string 类型
func: 函数，Function 类型
...: 可变参数 
返回值： 
table 类型
```

```lua
FunctionRemove(name)

移除注册函数。（仅lua模式使用）
参数：无
返回值： 
number 类型
```

```lua
ConfigGetString(section, key, def)
ConfigGetNumber(section, key, def)
ConfigGetBool(section, key, def)

获取ini文件配置项目 
参数：
section: 节点
key: 字段
def: 默认值
返回值：
分别返回string、number、bool
```

##### 5.5.4.2连接操作类接口

```lua
SocketConnect(ip, port, protocol)

发起网络连接
参数：
ip: ip地址
port: 端口
protocol: 连接类型，0 tcp、1 udp、2 ssl、8 http、9 https
返回值：
成功返回连接句柄，失败返回nil。
如果成功返回，建立tcp连接时，连接正在进行，需要等待异步通知。建立udp连接，连接可以立即使用，不用等待异步通知
```

```lua
SocketSend(hsock, data) 或者 SocketSend(hsock, data, len)

向连接发送消息
参数：
hsock:  网络连接句柄， 由SocketConnet函数返回
data: 消息字符串
len:  发送消息长度，参数可选
返回值：无
```

```lua
SocketClose(hsock)

关闭连接
参数：
hsock: 网络连接句柄
返回值：无
```

```lua
HttpRequest(method, url, header, content, callback)

发起一个http短连接请求。（仅lua模式使用）
参数：
method: string 类型,请求方法
url:    string 类型,网址，如：http://127.0.0.1:1080/path
header: table 类型, http头信息
content: string 类型,http正文
callback: 回调函数，如 funtion http_callback(res, errmsg)
返回值：bool类型
```

```lua
SocketPeerAddrGet(hsock)

获取连接对端地址
参数：
hsock: 网络连接句柄， 由SocketConnet函数返回
返回值：
ip:     目标ip地址
port:   目标端口
```

```lua
SocketLocalAddrGet(hsock)

获取连接本地地址
参数：
hsock: 网络连接句柄， 由SocketConnet函数返回
返回值：
ip:     目标ip地址
port:   目标端口
```

```lua
SocketPeerAddrSet(hsock, ip, port)

设置udp连接目标地址，仅对udp或着kcp连接有效
参数：
hsock:  网络连接句柄， 由SocketConnet函数返回
ip:     目标ip地址
port:   目标端口
返回值：无
```

```lua
GetHostByName(name)

将域名通过DNS解析为IP地址
参数： 
name: string 类型，域名
返回： 
ip: string 类型，网络地址
```

```lua
KcpCreate(hsock, conv, mode)

将UDP连接升级为KCP连接
参数：
hsock:  UDP连接句柄
conv:   KCP连接号
mode:   流模式，0非流模式，1流模式
返回值：
bool类型, true或者false
```

```lua
KcpNodelay(hsock, nodelay, interval, resend, nc)

配置KCP
参数：
hsock:  为连接句柄，其余参数 与kcp参数一致
返回值：无
```

```lua
KcpWndsize(hsock, snwnd, rvwnd)

配置KCP窗口
参数：
hsock:  连接句柄
snwnd:  发送窗口
rcwnd:  接收窗口
返回值：无
```

```lua
KcpDebug(hsock)

获取KCP状态信息，用于debug
参数：
hsock:  连接句柄
返回值：
string类型
```

##### 5.5.4.3播放器控制类接口

```lua
PlayStop(...)

停止用户运行，一般在压测用户执行出错且不可恢复时调用，参数用于说明错误原因
参数：
...： 可变参数
返回值：无
```

```lua
PlayPause()

设置用户播放状态为暂停，与PlayNormal()成对使用
参数：无
返回值：无
```

```lua
PlayNormal()

设置用户播放状态为播放，与PlayPause()成对使用
参数：无
返回值：无
```

```lua
PlayFast(fast)

设置用户播放为快进模式
参数：
fast: bool 类型
返回值：无
```

```lua
PlayStep()

获取用户当前执行用例步骤序号，
参数：无
返回值：
number 类型
```

```lua
PlayBack(step)

回退至指定 用例步骤
参数：
step:   用例步骤序号
返回值：
number 类型
```

```lua
PlayStepSession()

获取用户当前执行用例步骤短连接key,大量短连接场景使用
参数：无
返回值：
number 类型
```

```lua
PlayStepNote()

获取用户当前执行用例步骤批注，自定义用例使用
参数：无
返回值：
string 类型
```

```lua
PlayAction(action)

执行指定动作，action 是 NOTHING = 0x00, DELAY = 0x01, SEND = 0x02, DROP = 0x04, PLAY = 0x20, PAUSE = 0x40, FAST = 0x80 等的复合值，这里的逻辑比较绕，后续详细说明
参数：
action: 复合参数，
返回值：
number类型，可能是 DELAY 、SEND 、 DROP 三者之一的参数 

入参详细说明：动作参数在不同的场合使用不同的组合，基本规则如下：
1. NOTHING 单独使用，不修改用户播放状态，返回DOSEND，
2. DELAY 单独使用，将用户设置为暂停状态，返回DELAY，一般情况下不使用这个值
3. 其余参数可以复合使用，但其中 SEND、DROP 不同时存在， PLAY 、 PAUSE 、FAST 两两不同时存在,返回 SEND 或者 DROP
返回值详细说明：
1. SEND 如果时发送事件需要发送消息，接收事件忽略返回值
2. DROP 如果时发送事件需要丢弃消息，不做发送，接收事件忽略返回值
3. DELAY 不要发送消息，等待条件满足时再发送
```

```lua
PlayNoStop()

将用户设置为非自动停止状态，此状态下用例执行到末尾，不会触发EventStop事件，即用户不会停止运行，配合 PlayOver 接口使用
参数：无
返回值：无
```

```lua
PlayOver()

判断用例是否执行到末尾，配合 PlayNoStop 接口，用于控制用户结束时机，返回 true 时将用户设置为播放状态或者调用PlayStop，即可结束用户运行
参数：无
返回值：
true 或 false
```

##### 5.5.4.4报表类接口

```lua
ReportOnline()
ReportOffline()

设置用户为在线/离线状态，用于统计在线人数，用户停止运行时会自动离线
参数：无
返回值：无
```

```lua
ReportCounter(name, value, counter_type, space_time)

向控制端上报自定义统计数据
参数：
name:   string类型,统计项名称
value:  number类型
counter_type: number类型,统计类型,1 间隔时间内计数总和,2 历史总和,4时间间隔内平均,8历史平均
space_time:  number类型，统计间隔(秒)
返回值：无
```

```lua
ReportAPIResponse(name, response_time)

向控制端上报接口响应时间
参数：
name:   string类型,接口名称
response_time:  number类型,响应时间,>=0表示正常，<0表示错误个数，如-3表示3个错误
返回值：无
```

```lua
ReportAPISend(name, flow, count)

向控制端上报接口发送次数及消耗流量
参数：
name:   string类型,接口名称
flow:   number类型,字节数
count:  number类型,次数
返回值：无
```

```lua
ReportAPIRecv(name, flow, count)

向控制端上报接口接收次数及消耗流量
参数：
name:   string类型,接口名称
flow:   number类型,字节数
count:  number类型,次数
返回值：无
```

```lua
ReportOverMessage(msg)

用户结束运行时向控制端上报自定义消息，限制64个字节以内
参数：
msg:    string类型
返回值：无
```

### 5.6 Python模式

&emsp;&emsp;SuperSheeps内嵌python3脚本语言，要求用户已经安装python环境

#### 5.6.1 用户并发管理

&emsp;&emsp;python模式下，采用面向对象的方式进行用户管理，每个模拟用户对应一个类实例，为确保用户数据不会相互干扰，应该把用户数据保存到自身的类中。在sheeps.py文件中 定义了SheepsUserBase基类，用户需要定义一个名为User的类，继承基类，并实现固定的几个成员函数。(**注意，User类名是固定的，使用其他名字，SuperSheeps将无法识别**)

#### 5.6.2 集成第三方库

&emsp;&emsp;无内置第三方库，与用户python环境一致，用户可自行使用pip工具安装

#### 5.6.3 python项目模板

```python
#file main.py
from sheeps import SheepsUserBase

class User(SheepsUserBase):    #类名User不能变，框架默认的
    def __init__(self):
        pass

    def EventStart(self,task):
        pass

    def EventConnectOpen(self, ip, port, conn_type):
        pass

    def EventConnectMade(self, hsock):
        pass

    def EventConnectFailed(self, hsock):
        pass

    def EventConnectClose(self, ip, port, conn_type):
        pass

    def EventConnectClosed(self, hsock):
        pass

    def EventConnectSend(self, ip, port, content, conn_type):
        pass

    def EventConnectRecved(self, hsock, data):
        pass

    def EventTimeOut(self):
        pass

    def EventStop(self):
        pass
```

#### 5.6.4 python接口

&emsp;&emsp;SheepsUserBase类中定义了接口实现，命名与使用方法lua接口一致

### 5.7 Javascript模式（beta）
&emsp;&emsp;SuerSheeps内嵌quickjs引擎，支持使用javascript，使用方式及API请参考lua模式。（当前功能为beta版本）

### 5.8 Golang模式

&emsp;&emsp;SuperSheeps提供了abi接口供其他编译型语言使用，目前已经支持在windows和linux下使用golang进行开发  
&emsp;&emsp;项目： <https://gitee.com/lutianming/gsupersheeps.git>

### 5.9 ABI 桥接模式

&emsp;&emsp;SuperSheeps核心功能均包含在动态库中（win下Sheeps.dll,linux下libsheeps.so）,并提供了供用户扩展的abi接口，golang支持即是采用的这种模式，可以扩展支持其他编程语言，abi接口文档待补充完善，或阅读golang插件源码。

## 6.配置文件

&emsp;&emsp;SueperSheeps通过ini文件管理配置项

### 6.1 系统配置

&emsp;&emsp;sheeps.ini配置说明

```sheeps.ini
[common]
;控制端ip和端口配置
srv_ip = 127.0.0.1
srv_port = 1080

[project]
;项目配置，从0递增
0 = Web(http/websocket)

[parms]
;项目参数
#0 = parm1:bool;parm2:text;parm3:,1,2,3
0 = PlayFast:bool;RunCount:text

[server]
;控制端扩展脚本
script = server/init.lua

;是否开启代理
#proxy_open = 0

;ssl端口号，录制ssl流量时需要开启
#proxy_ssl_ports = 443

;控制端首页
#home_page = /SuperSheeps.html

[agent]
;是否开启资源同步
syncfile = 0

;项目编号
project_id = 0

;执行组编号
#group_id = 默认

;用户定时器超时时间，根据业务需要调整，单位毫秒
timer = 200

;初始化用户数量，用户初始化耗时过长可以使用此配置，以满足并发性能
user_count = 10

;打印日志到控制台，仅在调试时使用，控制台输出过多严重影响性能
log_stdout = 1

;是否开启静态IP，提供多个IP地址以突破单机连接数限制，或者用于服务器对单个IP连接数量有限制的场景
;原理则是本机设置为静态IP地址，并手动为网卡分配多个IP地址
;将这些IP地址写入ips.txt文件，放置于程序根目录，每行一个IP地址
static_ip = 1
```

### 6.2 自定义配置

&emsp;&emsp;SuperSheeps负载端启动时可以通过 -c 参数指定用户需要加载的配置文件，并且该配置会覆盖sheeps.ini中已经存在的配置


## 7.控制端扩展

&emsp;&emsp;Supersheeps控制端支持lua脚本扩展，可实现高度自定义功能

### 7.1 web静态文件服务

&emsp;&emsp;SuperSheeps内置web静态文件服务器，可以托管web服务器静态页面及文件，同时可以自定义首页，修改sheeps.ini中server -> home_page配置即可。·

### 7.2 web动态接口服务

&emsp;&emsp;SuperSheeps控制端内嵌lua虚拟机，通过http_handler.lua脚本可以处理任意自定义的http接口。

### 7.3 控制端事件通知

&emsp;&emsp;Supersheeps控制端支持部分事件通知，方便对接第三方系统，定义在event_handler.lua中。事件包含任务开始和停止、负载端连接和断开四种事件

### 7.4 控制端逻辑扩展

&emsp;&emsp;Supersheeps控制端启动时会调用init.lua脚本执行初始化，用户代码可以在此执行，可使用协程

### 7.5 控制端lua接口

&emsp;&emsp;SuperSheeps控制端内部实现了网络编程接口和休眠接口，该接口内部实现为异步和协程，防止阻塞线程

```lua
-- tcp服务端示例
local function server()
	local listener = net.listen("::", 9000)
	Log(2, "listen", listener)
	while listener do
		local tcp = listener:accept()
		Log(2, "accept", tcp)

		local function server_handle(tcp)
			while tcp do
				local msg = tcp:recv()
				if msg == "" then
					break
				end
				Log(2, "server recv:", msg)
				-- print("server recv:"..msg)
				tcp:send(msg)
			end
			tcp:close()
			Log(2, "server close")
		end

		local co = coroutine.create(server_handle)
		coroutine.resume(co, tcp)
	end
end

-- tcp客户端示例
local function client()
	local tcp = net.connect("::1", 9000, 0)
	Log(2, "connect", tcp)

	local client_connected = true
	local function logic()
		local i = 0
		while client_connected do
			tcp:send("hello world ".. i)
			Log(2, "client send", "hello world ".. i)
			i = i + 1
			sleep(5000)
		end
	end

	local co = coroutine.create(logic)
	coroutine.resume(co)

	while tcp do
		local msg = tcp:recv()
		if msg == "" then
			break
		end
		--Log(2, "client recv:", msg)
	end
	tcp:close()
	client_connected = false
	Log(2, "client close")
end
```
