---
title: "全链路性能测试SuperSheeps测试报告"
meta: <meta name="description" content="基于录制回放的游戏服务器及web性能测试专家"/>
---

## SuperSheeps测试报告

![微信截图_20230420110344](/assets/微信截图_20230420110344.png)

在任务执行过程中，会生成实时报表，其中部分数据为常规统计项，部分由用户自定义，如自定义计数、自定义过程耗时统计。

![微信截图_20230420161546](/assets/微信截图_20230420161546.png)

### 1. 自定义数量统计

![微信截图_20230420105648](/assets/微信截图_20230420105648.png)

```lua
功能接口：ReportCounter(name, value, counter_type, space_time)

向控制端上报自定义统计数据
参数：
name:   string类型,统计项名称，自定义
value:  number类型
counter_type: number类型,统计类型,1 间隔时间内计数总和,2 历史总和,4时间间隔内平均,8历史平均
space_time:  number类型，统计间隔(秒)
返回值：无
```

此接口用于创建和统计任意名称的计数器，支持四种统计类型：一定时间内数量总和、历史时间内总和、一定时间内平均值、历史时间内平均值。

### 2. 自定义过程统计

![微信截图_20231001160150](/assets/微信截图_20231001160150_2inmbb42n.png)

```lua
功能接口：ReportAPIResponse(name, response_time)

向控制端上报接口响应时间
参数：
name:   string类型,接口名称
response_time:  number类型,响应时间,>=0表示正常，<0表示错误个数，如-3表示3个错误
返回值：无
```
```lua
功能接口：ReportAPISend(name, flow, count)

向控制端上报接口发送次数及消耗流量
参数：
name:   string类型,接口名称
flow:   number类型,字节数
count:  number类型,次数
返回值：无
```

```lua
功能接口：ReportAPIRecv(name, flow, count)

向控制端上报接口接收次数及消耗流量
参数：
name:   string类型,接口名称
flow:   number类型,字节数
count:  number类型,次数
返回值：无
```

此接口用于创建和统计任意接口名称或者过程的时间消耗，并支持统计接口错误数量。

### 3. 在线人数统计

![微信截图_20230420105807](/assets/微信截图_20230420105807.png)

```lua
功能接口：ReportOnline()
功能接口：ReportOffline()

设置用户为在线/离线状态，用于统计在线人数，用户停止运行时会自动离线
参数：无
返回值：无
```

用于在人数统计报表中统计在线人数

### 4. 用户结束运行附加信息

![微信截图_20230420105931](/assets/微信截图_20230420105931.png)

```lua
功能接口：ReportOverMessage(msg)

用户结束运行时向控制端上报自定义消息，限制64个字节以内
参数：
msg:    string类型
返回值：无
```

只能在用户运行结束，即触发EventStop时调用此接口，上报一段简要信息，如用户账号id等，方便对问题账号进行排查
