local router = {}

router["/"] = function(conn, req)  --接口路由，即使不写该请求也会被默认处理
    Log(LOG_NORMAL, "http请求：", req)
    --PlayPause()   --发送请求之后暂停执行，等待本次返回
    --conn.ssl = true  --启用ssl连接

    conn.on_response = function(conn, res)  --http接口返回回调
        Log(LOG_NORMAL, "http 返回:", res.status_code, res.status)
		--PlayNormal()  --接收返回后继续执行
    end

    conn.on_ws_request = function(conn, message)   --websocket发送回调
        Log(LOG_NORMAL, "websocket 发送:", message)
        return message
    end
    
    conn.on_ws_response = function(conn, message)   --websocket接收回调
        Log(LOG_NORMAL, "websocket 接收:", message)
    end

    conn.on_close = function(conn, err)  --连接关闭回调函数
        Log(LOG_NORMAL, "http关闭:", err)
    end

end

-- router["*"] = function(http, req)   --这个路由用于接收所有未知的接口，用户可以更加自由的组织自己的代码
--     print(req.uri)
-- end

return router