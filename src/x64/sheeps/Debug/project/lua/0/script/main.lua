
--http插件使用示例

local http_controller = require "http_plugin.controller"
local router = require "router"

function EventStart(task)
	Log(LOG_NORMAL,"EventStart", task)
    local paly_fast = false
    local run_count = 1
    local params = task.params
    if params.PlayFast then paly_fast = true end
    if params.RunCount then run_count = tonumber(params.RunCount) end
    http_controller.init(router, paly_fast, run_count)  --初始化，设置路由、播放模式、及执行次数
end

function EventConnectOpen(ip, port, protocol)
    Log(LOG_DEBUG,"EventConnectOpen")
    http_controller.connection_open(ip, port, protocol)
end

function EventConnectClose(ip, port, protocol)
    Log(LOG_DEBUG,"EventConnectClose")
    http_controller.connection_close(ip, port, protocol)
end

function EventConnectSend(ip, port, data, protocol)
    Log(LOG_DEBUG,"EventConnectSend")
	http_controller.connection_send(ip, port, data, protocol)
end

function EventConnectMade(hsock)
    Log(LOG_DEBUG,"EventConnectMade")
    http_controller.connection_made(hsock)
end

function EventConnectFailed(hsock, err)
    Log(LOG_DEBUG,"EventConnectFailed")
    http_controller.connection_failed(hsock)
end

function EventConnectClosed(hsock, err)
    Log(LOG_DEBUG,"EventConnectClosed")
    http_controller.connection_closed(hsock, err)
end

function EventConnectRecved(hsock, data)
    Log(LOG_DEBUG,"EventConnectRecved data, len", #data)
    --Log(3, "Tcp Socket Connect Recved:", #data)
    http_controller.connection_recved(hsock, data)
end

function EventTimeOut()
    --Log(LOG_DEBUG,"EventStart")
    http_controller.timeout()
end

function EventStop(msg)
	Log(LOG_FAULT,"EventStop:",msg)
end