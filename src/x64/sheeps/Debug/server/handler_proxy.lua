function proxy_connect_open_hook(ctx)
    print("open", ctx.type, ctx.host, ctx.client_ip, ctx.client_port, ctx.server_ip, ctx.server_port)
end

function proxy_connect_faile_hook(ctx)
    print("faile", ctx.type, ctx.client_ip, ctx.client_port, ctx.server_ip, ctx.server_port)
end

function proxy_connect_close_hook(ctx)
    print("close", ctx.type, ctx.client_ip, ctx.client_port, ctx.server_ip, ctx.server_port)
end

function proxy_connect_send_hook(ctx, data)
    print("send", ctx.type, ctx.client_ip, ctx.client_port, ctx.server_ip, ctx.server_port)
    return 0
end

function proxy_connect_recv_hook(ctx, data)
    print("recv", ctx.type, ctx.client_ip, ctx.client_port, ctx.server_ip, ctx.server_port)
    return 0
end