local json = require "JSON"
local host = "http://127.0.0.1:1080"
local event_router = {}

local function http_post(uri, req)
	local res, err = net.http_request("POST", host..uri, nil, json:encode(req))
	if res then
		local info = json:decode(res.content)
		return info.data
	end
	return nil
end

event_router["task_start"] = function(task)	-- 任务开始前置处理
end

event_router["task_stop"] = function(task)  --任务结束后置事件处理
	local parm = { task_id = task.task_id }

	-- 获取任务配置
	local task_info = http_post("/api/task_info", parm)
	Log(2, task_info)

	-- 获取任务报告统计
	local report_info = http_post("/api/report_info", parm)
	Log(2, report_info)

	-- 获取人数统计信息
	parm.index = 0
	parm.count = report_info.online.total
	--local parm = { task_id = task.task_id, index = 0, count = 10000 }
	local online = http_post("/api/report_online", parm)
	Log(2, online)

	-- 获取连接统计信息
	parm.count = report_info.connect.total
	--local parm = { task_id = task.task_id, index = 0, count = 10000 }
	local connect = http_post("/api/report_connect", parm)
	Log(2, connect)

	-- 获取网络统计信息
	parm.count = report_info.net.total
	--local parm = { task_id = task.task_id, index = 0, count = 10000 }
	local network = http_post("/api/report_net", parm)
	Log(2, network)

	-- 获取自定义统计信息
	parm.count = report_info.custom.total
	--local parm = { task_id = task.task_id, index = 0, count = 10000 }
	local custom = http_post("/api/report_custom", parm)
	Log(2, custom)

	-- 获取接口统计信息
	--local parm = { task_id = task.task_id }
	local api = http_post("/api/report_api", parm)
	Log(2, api)

	-- 获取接口响应时间详细信息,非必要
	--local parm = { task_id = task.task_id, index = 0, count = 10000, api = api_name }
	-- local api_info = http_post("/api/report_api_info", parm)
	-- Log(2, api_info)

	-- 获取运行结果统计信息
	parm.count = report_info.error.total
	--local parm = { task_id = task.task_id, index = 0, count = 10000 }
	local err = http_post("/api/report_error", parm)
	Log(2, err)

		
	-- 整合报表信息并存储为html
	local report_data = {
		task_info = task_info, report_info = report_info,  online = online, 
		connect = connect, net = network, custome = custom,
		api = api, error = err}
	local sorce_data = json:encode(report_data)
	-- local file = io.open("report_data.txt", "a")
    -- file:write(sorce_data)
    -- file:write("\r\n")
    -- file:close()
	local res, error = net.http_request("GET", host.."/report_template", nil, "")
	if res then
		local info = res.content:gsub("report_sorce_data", sorce_data)
		local info = info:gsub("jquery.min.js", "../jquery.min.js")
		local info = info:gsub("echarts.min.js", "../echarts.min.js")

		local file_name = task_info.project_name.."_"..os.date("%Y_%m_%d_%H_%M_%S", report_info.start_time).."_"..task_info.des..".html"
		file_name = file_name:gsub(" ", "_")
		file_name = file_name:gsub("/", "_")
		file_name = file_name:gsub("\\", "_")

		local file, err = io.open("report", "rb")
		if file == nil then
		 	if err:find("No such file or directory") ~= nil then
				os.execute('mkdir report')
			end
		else
			file:close()
		end
		
		local file, err = io.open("report/"..file_name, "wb")
        file:write(info)
        file:write("\r\n")
        file:close()
	end
end

event_router["agent_connect"] = function(agent)	-- 负载端连接事件
end

event_router["agent_disconnect"] = function(agent)	-- 负载端断开连接事件
end

function event_handler(event, data)
	Log(2, event, data)
	event_router[event](data)
end
