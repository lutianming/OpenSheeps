/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#ifndef __SHEEPS_H__
#define __SHEEPS_H__

#if !defined(__WINDOWS__) && (defined(WIN32) || defined(WIN64) || defined(_MSC_VER) || defined(_WIN32))
#define __WINDOWS__
#endif

#ifdef __WINDOWS__
#define __STDCALL __stdcall
#define __CDECL__	__cdecl
#if defined SHEEPS_EXPORTS
#define Sheeps_API __declspec(dllexport)
#else
#define Sheeps_API __declspec(dllimport)
#endif
#else
#define __STDCALL
#define __CDECL__
#define Sheeps_API
#endif // __WINDOWS__

//#define PYTHON_SUPPORT

bool sheeps_aready_run();

extern "C" Sheeps_API int __STDCALL run_with_parms(int argc, char* argv[]);

#endif
