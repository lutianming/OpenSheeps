#include "BridgeProtocol.h"
#include "sheeps.h"
#include "lserverhook.h"

func_event_start_callback start_call;
func_event_connect_open_callback open_call;
func_event_connect_made_callback made_call;
func_event_connect_failed_callback failed_call;
func_event_connect_send_callback send_call;
func_event_connect_recved_callback recv_call;
func_event_connect_close_callback close_call;
func_event_connect_closed_callback closed_call;
func_event_timeout_callback timeout_call;
func_event_stop_callback stop_call;

func_task_user_create user_create;
func_task_start task_start;
func_task_stop task_stop;

char bridge_config[256] = {0x0};

void __STDCALL set_task_callback(func_task_user_create create, func_task_start start, func_task_stop stop) {
	user_create = create; task_start = start; task_stop = stop;
}
void __STDCALL set_user_event_callback(func_event_start_callback start, func_event_connect_open_callback oepn, func_event_connect_made_callback made,
	func_event_connect_failed_callback failed, func_event_connect_send_callback send, func_event_connect_recved_callback recv,
	func_event_connect_close_callback close, func_event_connect_closed_callback closed, func_event_timeout_callback timeout,
	func_event_stop_callback stop
	) {
	start_call = start; open_call = oepn; made_call = made; failed_call = failed; send_call = send;
	recv_call = recv; close_call = close; closed_call = closed; timeout_call = timeout; stop_call = stop;
}

static void TaskStart(hClientTaskConfig task){
	config_init(bridge_config);
	task_start(task->taskID, task->projectID, task->machineID, task->run_number, task->parms);
}
static void TaskStop(hClientTaskConfig task){
	task_stop(task->taskID);
}

static ReplayProtocol* CreateUser(void){return new BirdgeProtocol;}

BirdgeProtocol::BirdgeProtocol() {}
BirdgeProtocol::~BirdgeProtocol() {}
void BirdgeProtocol::EventStart() {
	if (!user) user = user_create(this, Task->taskID);
	start_call(user, Task->taskID, UserNumber);
}
void BirdgeProtocol::EventConnectOpen(const char* ip, int port, PROTOCOL protocol) {
	open_call(user, ip, port, protocol);
}
void BirdgeProtocol::EventConnectMade(HSOCKET hsock) {
	made_call(user, hsock);
}
void BirdgeProtocol::EventConnectFailed(HSOCKET hsock, int err) {
	failed_call(user, hsock, err);
}
void BirdgeProtocol::EventConnectClose(const char* ip, int port, PROTOCOL protocol) {
	close_call(user, ip, port, protocol);
}
void BirdgeProtocol::EventConnectClosed(HSOCKET hsock, int err) {
	closed_call(user, hsock, err);
}
void BirdgeProtocol::EventConnectSend(const char* ip, int port, const char* content, int clen, PROTOCOL protocol) {
	send_call(user, ip, port, content, clen, protocol);
}
void BirdgeProtocol::EventConnectRecved(HSOCKET hsock, const char* data, int len) {
	recv_call(user, hsock, data, len);
	HsocketPopBuf(hsock, len);
}
void BirdgeProtocol::EventTimeOut() {
	timeout_call(user);
}
void BirdgeProtocol::EventStop() {
	stop_call(user, this->stop_reason);
}

void BirdgeProtocol::EventStopSignal() {
}

int __STDCALL TaskManagerRunWithBridge(int projectid, const char* groupid, int local_server, const char* configfile){
	if (sheeps_aready_run())return -1;
	if (local_server){
		server_hook_set();
	}
	task_agent_welcome_set("欢迎使用SuperSheeps for Bridge");
	snprintf(bridge_config, sizeof(bridge_config), "%s", configfile);
	TaskManagerRun(projectid, groupid, local_server, NULL, 0, NULL, NULL, CreateUser, TaskStart, TaskStop);
	return 0;
}