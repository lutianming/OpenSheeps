﻿#include "lua_3rd.h"
#include "lptree.h"
#include "pb.h"
#include "luasql.h"

static const luaL_Reg loadedlibs[] = {
    {"lpeg", luaopen_lpeg},
    {"sproto.core", luaopen_sproto_core},
    {"crypt", luaopen_client_crypt},
    {"pb", luaopen_pb},
    {"pb.unsafe", luaopen_pb_unsafe},
    {"pb.io", luaopen_pb_io},
    {"pb.buffer", luaopen_pb_buffer},
    {"pb.slice", luaopen_pb_slice},
    {"pb.conv", luaopen_pb_conv},
    {NULL, NULL}
};


LUALIB_API void luaL_open_3rd_lib(lua_State* L) {
    const luaL_Reg* lib;
    /* "require" functions from 'loadedlibs' and set results to global table */
    for (lib = loadedlibs; lib->func; lib++) {
        luaL_requiref(L, lib->name, lib->func, 1);
        lua_pop(L, 1);  /* remove lib */
    }
}

static const luaL_Reg loadedlibs_server[] = {
    {"luasql.sqlite3", luaopen_luasql_sqlite3},
    {NULL, NULL}
};


LUALIB_API void luaL_open_3rd_server_lib(lua_State* L) {
    const luaL_Reg* lib;
    /* "require" functions from 'loadedlibs' and set results to global table */
    for (lib = loadedlibs_server; lib->func; lib++) {
        luaL_requiref(L, lib->name, lib->func, 1);
        lua_pop(L, 1);  /* remove lib */
    }
}