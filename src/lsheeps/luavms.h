/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#ifndef __LUAVMS_H__
#define __LUAVMA_H__
#include "lua.hpp"
#include "LuaProtocol.h"

#define _UserProtoName "_user_proto_name"
#define _FunctionCall "_function_callbak"
#define _Task "_task_"
#define _htask "htask"
#define _taskId "task_id"
#define _runNumber "run_number"
#define _machineId "mac_id"
#define _projectId "project_id"
#define _parms "params"
#define _User "_user_"
#define _UserNumber "user_number"

#define _SHEEPL_LUA "sheeps"
#define _GlobalVmInit "global_vm_init"
#define _LuavmInit "user_vm_init"

extern lua_State* globle_L;

lua_State* create_lua_State();
int sheeps_report(lua_State* L, int status, LuaProtocol* proto, bool stop, const char* func, int line);
void stop_signal_hook(lua_State* L, lua_Debug* ar);

int set_global(lua_State* L, LuaProtocol* proto);
int init_luavms();
int init_main_script(lua_State* L, LuaProtocol* proto);

void reset_luavm(lua_State* L);

#endif // !__LUAVMS_H__