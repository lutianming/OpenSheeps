/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#ifndef _SERVER_PROXY_H_
#define _SERVER_PROXY_H_
#include "ServerProtocol.h"

int	 CheckPoxyRequest(HSOCKET hsock, ServerProtocol* proto, const char* data, int len);
void ProxyConnectionMade(HSOCKET hsock, ServerProtocol* proto);
void ProxyConnectionFailed(HSOCKET hsock, ServerProtocol* proto);
void ProxyConnectionClosed(HSOCKET hsock, ServerProtocol* proto);

#endif // !_SERVER_PROXY_H_