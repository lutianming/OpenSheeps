/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#include "framework.h"
#include "SheepsFactory.h"
#include "ServerProtocol.h"
#include "AgentProtocolSub.h"
#include "ServerStress.h"
#include "ServerCase.h"
#include "ServerSocksProxy.h"
#include "ServerHook.h"
#include "AgentManager.h"

static EchoUdpProtocol* EchoUdp = NULL;
static HSOCKET EchoUdpSock = NULL;

class EchoUdpProtocol : public BaseWorker{
	void ConnectionMade(HSOCKET hsock, PROTOCOL protocol) {}
	void ConnectionFailed(HSOCKET hsock, int err) {}
	void ConnectionClosed(HSOCKET hsock, int err) {}
	void ConnectionRecved(HSOCKET hsock, const char* data, int len) {
		HsocketSend(hsock, data, len);
		HsocketPopBuf(hsock, len);
	}
};

SheepsAccepter::SheepsAccepter(){
}

SheepsAccepter::~SheepsAccepter(){
}

bool SheepsAccepter::Init(int server_port){
	bool ret = true;
	if (!AgentProtocolInit()) ret = false;
	if (server_port > 0){
		EchoUdp = new(std::nothrow) EchoUdpProtocol;
		EchoUdp->auto_release(false);
		EchoUdp->thread_set();
		EchoUdpSock = HsocketListenUDP(EchoUdp, "::", 1080);

		if (!ServerInit(ConfigFile)) ret = false;
		if (!server_default_case_init()) ret = false;
		if (!StressServerInit()) ret = false;
		server_hook_create();
	}
	return ret;
}

bool SheepsAccepter::Inited() {
	server_hook_init();
	return true;
}

void SheepsAccepter::TimeOut(){
	NOWTIME = time(NULL);
	LogLoop();
	insert_msg_recodr_db();
}

BaseWorker* SheepsAccepter::GetWorker(){
	BaseWorker* proto = new(std::nothrow) ServerProtocol();
	proto->thread_set(0);
	return proto;
}