/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#include "framework.h"

#ifdef __WINDOWS__
#include "direct.h"
HMODULE Sheeps_Module;
#endif // __WINDOWS__

#define MORE_PATH MAX_PATH+32
char EXE_Path[MAX_PATH] = { 0x0 };
char ConfigFile[MORE_PATH] = { 0x0 };
char ProjectPath[MORE_PATH] = { 0x0 };
char CasePath[MORE_PATH] = { 0x0 };
//char ReportPath[MORE_PATH] = { 0x0 };
char LogPath[MORE_PATH] = { 0x0 };

bool ShowConsole = false;

time_t NOWTIME = time(NULL);

#ifdef __WINDOWS__
static void set_console_mode() {
	HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
	DWORD mode;
	GetConsoleMode(hStdin, &mode);
	mode &= ~ENABLE_QUICK_EDIT_MODE; //禁用快速编辑模式
	mode &= ~ENABLE_INSERT_MODE; //禁用插入模式
	mode &= ~ENABLE_MOUSE_INPUT;
	SetConsoleMode(hStdin, mode);
}
#endif

int framwork_init(void* Module, const char* root_path) 
{
	snprintf(EXE_Path, sizeof(EXE_Path), "%s", root_path);
#ifdef __WINDOWS__
	set_console_mode();

	Sheeps_Module = (HMODULE)Module;
	snprintf(ConfigFile, sizeof(ConfigFile), "%ssheeps.ini", root_path);
	snprintf(ProjectPath, sizeof(ProjectPath), "%sproject", root_path);
	snprintf(CasePath, sizeof(CasePath), "%scase", root_path);
	//snprintf(ReportPath, sizeof(ReportPath), "%sreport\\", root_path);
	snprintf(LogPath, sizeof(LogPath), "%slog", root_path);
	_mkdir(ProjectPath);
	_mkdir(CasePath);
	//_mkdir(ReportPath);
	_mkdir(LogPath);
#else
    snprintf(ConfigFile, sizeof(ConfigFile), "%ssheeps.ini", root_path);
	snprintf(ProjectPath, sizeof(ProjectPath), "%sproject", root_path);
	snprintf(CasePath, sizeof(CasePath), "%scase", root_path);
	//snprintf(ReportPath, sizeof(ReportPath), "%sreport/", EXE_Path);
	snprintf(LogPath, sizeof(LogPath), "%slog", root_path);
	mkdir(ProjectPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	mkdir(CasePath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	//mkdir(ReportPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	mkdir(LogPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#endif
	config_init(ConfigFile);
	return 0;
}