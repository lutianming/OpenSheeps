/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#ifndef _SHEEPS_FACTORY_H_
#define _SHEEPS_FACTORY_H_
#include "AgentProtocol.h"

class EchoUdpProtocol;

class SheepsAccepter :
	public BaseAccepter
{
public:
	SheepsAccepter();
	~SheepsAccepter();
	bool	Init(int server_port);
	bool	Inited();
	void	TimeOut();
	BaseWorker*	GetWorker();
};

#endif // !_SHEEPS_FACTORY_H_
