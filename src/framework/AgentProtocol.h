/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#ifndef _CLIENT_PROTOCOL_H_
#define _CLIENT_PROTOCOL_H_
#include "framework.h"
#include <list>
#include <vector>
#include <mutex>

#ifdef __WINDOWS__
#include "pdh.h"
#endif

typedef struct {
	int task_id;
	int mac_id;
	std::vector<std::string>* files;
	size_t index;
	//int size;
	//int len;
	FILE* hfile;
}TaskReportLogInfo;

class AgentProtocol :
	public BaseWorker
{
public:
	AgentProtocol();
	~AgentProtocol();

public:
	bool			authed = false;
	unsigned char	heartbeat = 0;
	HSOCKET			StressHsocket = NULL;

public:
	void ConnectionMade(HSOCKET hsock, PROTOCOL protocol);
	void ConnectionFailed(HSOCKET hsock, int err);
	void ConnectionClosed(HSOCKET hsock, int err);
	void ConnectionRecved(HSOCKET hsock, const char* data, int len);
	void Auth(HSOCKET hsock);
	int	 Loop();

	int  CheckRequest(HSOCKET hsock, const char* data, int len);
	void TaskReport();
	void TaskTeportLogFile(TaskReportLogInfo* info);
	void TaskTeportLogInsert(int taskid, int projectid, int macid);
	void Heartbeat();
	
};
#define AGENTPROTOCOL_SIZE sizeof(AgentProtocol)

extern int				AgentSyncFile;
extern AgentProtocol*	AgentProto;
extern char				AgentManagerIP[40];
extern unsigned short	AgentManagerPort;
extern uint8_t			AgentProjectid;
extern char				AgentGroupid[32];

extern int					AgentStaticIP_SIZE;
extern std::vector<char*>	AgentStaticIP;

extern std::map<int, TaskReportLogInfo*> ReportLogInfos;

int agent_virtual_ip_load();
bool AgentProtocolInit();

#endif // !_CLIENT_PROTOCOL_H_