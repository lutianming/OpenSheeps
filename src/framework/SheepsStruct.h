/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#ifndef _SHEEPS_STRUCT_H_
#define _SHEEPS_STRUCT_H_

typedef struct {
	int msgLen;
	short cmdNo;
	short binary;
}t_stress_protocol_head;

#define SHEEPS_HEAD_SIZE sizeof(t_stress_protocol_head)
#define SHEEPS_SSL

enum {
	C2S_Agent_Hertbaet = 0,
	C2S_Agent_Report_Info,
	S2C_Agent_File_Sync,
	C2S_Agent_Download_File,
	C2S_Agent_File_Sync_Done,
	C2S_Agent_Report_Date,

	S2C_Task_Init = 10,
	S2C_Task_Add_User,
	S2C_Task_Add_Case,
	S2C_Task_Stop_Case,
	S2C_Task_Finish,
	C2S_Task_Init_Error,
	S2C_Task_Log_Level,
	C2S_Task_Report_Events,
	C2S_Task_Report_Log,
	C2S_Task_Report_Log_Done
};

enum {
	Report_User_Stop = 0,
	Report_User_Online,
	Report_User_Offline,
	Report_User_Connect,
	Report_User_Netflow,
	Report_User_Api,
	Report_User_Custom
};

enum {
	PER_SUM = 0x01,
	HIS_SUM = 0x02,
	PER_AVG = 0x04,
	HIS_AVG = 0x08
};

#define DEFAUT_GROUP "默认"

#endif // !_SHEEPS_STRUCT_H_