#pragma once
#include "ServerProtocol.h"

extern bool Proxy_record;
extern sqlite3* mysql;
extern const char* record_dir;
extern const char* record_database;

typedef struct {
	unsigned long long	sessionid;
	long long	time;  //微秒
	char	ip[40];
	int		port;
	int		event;
	int		protocol;
	char*	msg;
	int		msglen;
}t_recode_info;

extern std::list<t_recode_info*>* recordList;
extern std::map<std::string, bool>* record_filter;

const char* get_project_name_by_project_id(int project_id);

int create_case_name(int project_id, char* buf, size_t size);
void case_info_init(sqlite3* dbConn, int projectid, const char* project, const char* des);
void case_info_update(sqlite3* dbConn, int projectid, const char* project, const char* des);
int case_append(sqlite3* dbconn, long long time, int event_type, int protocol, const char* ip, int port, unsigned long long sessionid, const char* content, int clen, const char* note, int nlen);

bool server_default_case_init();

bool default_case_change_name(const char* case_dir, const char* new_name);

void insert_msg_to_record_list(const char* ip, int port, unsigned long long sessionid, int event_type, int protocol, const char* msg, int len);
void insert_msg_recodr_db();

int ip_port_to_table(const char* ip, int port, char* buf, size_t size);
int addr_to_table(const char* addr, char* buf, size_t size);
int table_to_addr(const char* table, char* buf, size_t size);