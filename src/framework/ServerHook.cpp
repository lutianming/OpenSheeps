#include "ServerHook.h"
#include "SheepsFactory.h"

ServerCallbackCreate server_hook_create_func = NULL;
ServerHookProtocol* server_hook = NULL;

int shooklogid = -1;
std::map<std::string, PorxyInfoApi*> ProxyApi;

static int hook_log_init() {
	const char* file = config_get_string_value("server", "log_file", "server");

	char fullpath[256] = { 0x0 };
	snprintf(fullpath, sizeof(fullpath), "%s/%s", LogPath, file);
	int loglevel = config_get_int_value("server", "log_level", LOG_NORMAL);
	int maxsize = config_get_int_value("server", "log_size", 20);
	int timesplit = config_get_int_value("server", "log_time", 0);
	int maxfile = config_get_int_value("server", "log_maxfile", 1);
	shooklogid = RegisterLog(fullpath, loglevel, maxsize, timesplit, maxfile);
	return shooklogid;
}

void server_hook_create() {
	if (!server_hook && server_hook_create_func){
		if (hook_log_init() < 0) return;
		server_hook = server_hook_create_func();
		if (server_hook) {
			server_hook->auto_release(false);
			server_hook->thread_set(0);  //和ServerPorotocol在同一个线程工作
		}
	}
}

void server_hook_init() {
	if (server_hook) {
		PostSignal(server_hook, 0, [](BaseWorker* proto, long long signal) {
			((ServerHookProtocol*)proto)->init();
		});
	}
}

void server_hook_reinit() {
	if (server_hook) {
		PostSignal(server_hook, 0, [](BaseWorker* proto, long long signal) {
			((ServerHookProtocol*)proto)->reinit();
			});
	}
}